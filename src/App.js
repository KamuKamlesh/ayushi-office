import React from 'react'
import DataTable from './components/pages/DataTable'
import TodoApp from './components/TodoApp'



function App() {
  

  return (
    <>
    <DataTable/>
      <TodoApp/>
    </>
  )
}

export default App
