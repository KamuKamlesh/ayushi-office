import React, {useState, useEffect} from 'react';
import { useTable } from 'react-table';
import { Paper, Checkbox, makeStyles, TableBody, TableRow, TableCell, Toolbar, InputAdornment } from '@material-ui/core';
import UseTable from './UseTable';

const data =   [{
    "id": 1,
    "jobNumber": 21,
    "client": "",
    "clientDate": "12/7/2020",
    "dealStatus": false,
    "designStatus": false,
    "administrativeStatus": true
  }, {
    "id": 2,
    "jobNumber": 70,
    "client": "",
    "clientDate": "10/15/2020",
    "dealStatus": false,
    "designStatus": false,
    "administrativeStatus": false
  }, {
    "id": 3,
    "jobNumber": 41,
    "client": "",
    "clientDate": "5/7/2020",
    "dealStatus": false,
    "designStatus": false,
    "administrativeStatus": true
  }, {
    "id": 4,
    "jobNumber": 21,
    "client": "",
    "clientDate": "9/30/2020",
    "dealStatus": true,
    "designStatus": false,
    "administrativeStatus": true
  }, {
    "id": 5,
    "jobNumber": 9,
    "client": "",
    "clientDate": "9/3/2020",
    "dealStatus": true,
    "designStatus": true,
    "administrativeStatus": true
  }, {
    "id": 6,
    "jobNumber": 78,
    "client": "",
    "clientDate": "4/19/2021",
    "dealStatus": false,
    "designStatus": true,
    "administrativeStatus": false
  }, {
    "id": 7,
    "jobNumber": 88,
    "client": "",
    "clientDate": "4/9/2021",
    "dealStatus": true,
    "designStatus": true,
    "administrativeStatus": false
  }, {
    "id": 8,
    "jobNumber": 50,
    "client": "",
    "clientDate": "4/26/2021",
    "dealStatus": true,
    "designStatus": true,
    "administrativeStatus": false
  }, {
    "id": 9,
    "jobNumber": 42,
    "client": "",
    "clientDate": "12/1/2020",
    "dealStatus": false,
    "designStatus": false,
    "administrativeStatus": false
  }, {
    "id": 10,
    "jobNumber": 60,
    "client": "",
    "clientDate": "11/4/2020",
    "dealStatus": false,
    "designStatus": true,
    "administrativeStatus": true
  }, {
    "id": 11,
    "jobNumber": 47,
    "client": "",
    "clientDate": "7/30/2020",
    "dealStatus": true,
    "designStatus": true,
    "administrativeStatus": false
  }, {
    "id": 12,
    "jobNumber": 68,
    "client": "",
    "clientDate": "12/15/2020",
    "dealStatus": true,
    "designStatus": true,
    "administrativeStatus": false
  }, {
    "id": 13,
    "jobNumber": 77,
    "client": "",
    "clientDate": "11/24/2020",
    "dealStatus": false,
    "designStatus": true,
    "administrativeStatus": true
  }, {
    "id": 14,
    "jobNumber": 43,
    "client": "",
    "clientDate": "4/10/2021",
    "dealStatus": false,
    "designStatus": true,
    "administrativeStatus": false
  }, {
    "id": 15,
    "jobNumber": 50,
    "client": "",
    "clientDate": "3/22/2021",
    "dealStatus": false,
    "designStatus": true,
    "administrativeStatus": true
  }]

const headCells = [
    { id: 'selectAll', label: 'Employee Name' },
    { id: 'jobNumber', label: 'Job Number' },
    { id: 'client', label: 'Client' },
    { id: 'clientDate', label: 'Client Date'},
    { id: 'dealStatus', label: 'Deal Status'},
    { id: 'designStatus', label: 'Design Status'},
    { id: 'administrativeStatus', label: 'Administrative Status'}
]

export default function TodoApp() {
    const [records, setRecords] = useState([]) 
    const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })


    useEffect(() => {
        setRecords(data, []);
    })


    const {
        TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPagingAndSorting
    } = UseTable(records, headCells, filterFn);

    return (
        <div>
            <TblPagination />
            <TblContainer>
                    <TblHead />
                    <TableBody>
                        {
                            recordsAfterPagingAndSorting().map(item =>
                                (<TableRow key={item.id}>
                                    <TableCell padding="checkbox">
                    <Checkbox
                      checked={-1 !== -1}
                      
                      value="true"
                    />
                  </TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    <TableCell>{item.client}</TableCell>
                                    
                                </TableRow>)
                            )
                        }
                    </TableBody>
                </TblContainer>
        </div>
    )
}


